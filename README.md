# One Obs Kinesis Connector

This repository is a general solution to connect to the OneObs streams. 

This is a sample template that you can use to process One Obs kinesis streams after you set up the cross-account permissions. This repository is built around this [kinesis client code](https://github.com/awslabs/amazon-kinesis-client-python/blob/master/samples/sample_kclpy_app.py) from AWS.

Keep in mind that you can have [duplicate processing](https://docs.aws.amazon.com/streams/latest/dev/kinesis-record-processor-duplicates.html).

The account id used in this repository points to the development account of the obs team. We need to manually give you permission to use the stream cross-account. Contact the team if you plan to connect to the stream.
## Prerequisites
Anything that can run docker containers (EC2, ECS, EKS, etc.).

## How to use

There is a sample Cloudformation template included here for running this inside an EC2 instance. That template will set up all the necessary permissions you need as long as your AWS Account has been whitelisted.

1. Fork or copy contents of this repository in your repository.
2. Under `app.properties`, you will have to change `applicationName`. This should be the same as `AppName` in `infra/asg.yml` file.
> **_NOTE:_**  It's really important you change the `applicationName`.
3. Put your code logic under `src/processor.py`.
4. The `src/main.py` file handles most of the things you have to consider when using a kinesis stream. You can leave it as is. It's a slightly modified version of
[this](https://github.com/awslabs/amazon-kinesis-client-python/blob/master/samples/sample_kclpy_app.py). You can modify it but be sure you know what you're doing.
5. To run, `sudo docker-compose up -d`.
6. Logs are sent to `/var/log/one-obs-connector.log`.

## Using AWS CLI
To use AWS CLI, include the following to `~/.aws/config`. Again, the account number in there is under `dtn-wx-dev`. Contact the team for production credentials.
```
[profile inter-account]
role_arn = arn:aws:iam::688714617473:role/kinesis-inter-account-obs
credential_source = Ec2InstanceMetadata
region = us-east-1
```
With this, you can now use aws cli regularly.

Example:
```
aws kinesis describe-stream --stream-name one-obs-dedup-normalized-dev --profile inter-account
```


## List of Kinesis Streams
We have a couple of streams available but we're only exposing one as of the moment.

* `one-obs-dedup-normalized-{dev|prd}`
    * Metadata is attached and observations are normalized to a consistent set of units (SI).

    ```python
    data = [
        {
            "station_id": "KRBO",
            "obs_timestamp": "2021-11-13T13:35:00",
            "data_source_id": "NOAA_METAR",
            "receive_timestamp": "2021-11-16T04:12:34",
            "raw_data_s3_location": "s3://one-obs-raw-data-local/NOAA_METAR/2021/11/16/04/12/299fe1114b25403e82074d1c9ffa463c.txt",
            "latitude": 27.77804,
            "longitude": -97.6878,
            "elevation": 24.0,
            "device_id": "1",
            "device_record_id": "dfba64068fdc0d0dcd6502dad0d7bcbf3f0e097f00fa9d0a941c1ba74a5b9935",
            "dtn_station_id": "1fb4d0ec-b20b-11eb-ab9c-0ad07224a539",
            "station_code": "YPXC1",
            "device_type": 69,
            "timezone": "America/Chicago",
            "geolocation_id": "88c3ae288c6612035670add39b794bf52001afa21f1b5aeee60b8620af1621eb",
            "data": [
                {
                    "source_key": "vis",
                    "value": 10.0,
                    "source_key_display": "vis",
                    "sensor_id": 0,
                    "type": "float",
                    "unit": "miles",
                    "dtn_key_id": 926,
                    "std_type": "float",
                    "std_unit": "km",
                    "std_value": 16.09,
                    "dtn_key_name": "visibility",
                    "obs_type": 29,
                    "json_doc": None,
                    "raw_data_s3_location": "s3://one-obs-raw-data-local/NOAA_METAR/2021/11/16/04/12/299fe1114b25403e82074d1c9ffa463c.txt",
                    "properties": {}
                },
                ...
            ]
        },
        ...
    ]
    ```

* (TBA) `one-obs-agg-enriched-{dev|prd}`
    * Observations with added derivations (dew point is derived from air temperature and relative humidity, etc.). The data is also structured differently. The format is not consumption-friendly as of the moment. If you need derivations, please
    let us know so we can work on it.

## Metadata
A couple of csv files are provided in this repository under `metadata/`. The files are about the data sources and the current list of parameters. Those files might update.

## Some useful notes
1. [Getting started with Amazon Kinesis Data Streams](https://aws.amazon.com/kinesis/data-streams/getting-started/?nc=sn&loc=3).
2. [Quick Start: Install and configure the CloudWatch Logs agent on a running EC2 Linux instance](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/QuickStartEC2Instance.html).
3. [Example: Read From a Kinesis Stream in a Different Account](https://docs.aws.amazon.com/kinesisanalytics/latest/java/examples-cross.html).
4. Send an email to <WeatherObs-Dev-Team-Manila@dtn.com> for any questions or help.

## Possible improvements
1. Metrics for monitoring (iterator age, etc.)
2. Increase parallelism on partition keys