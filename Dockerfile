FROM python:3.9-slim-buster
WORKDIR /app

# install java
RUN apt update && apt install -y default-jre

# install requirements
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# generates the command to run the app
RUN amazon_kclpy_helper.py --print_command --java /usr/bin/java --properties /app/app.properties > /app/run.sh

COPY . .

CMD ["bash", "/app/run.sh"]
