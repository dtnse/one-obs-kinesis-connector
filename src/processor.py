def process_records(logger, data, **kwargs):
    """put your logic here
    logger: logger
    data: list
    kwargs: dict - partition_key, sequence_number, subsequence_number
        If your application does not need to process these extra metadata, no need
        to think about them

    data structure
    >>> data = [
        {
            "station_id": "KRBO",
            "obs_timestamp": "2021-11-13T13:35:00",
            "data_source_id": "NOAA_METAR",
            "receive_timestamp": "2021-11-16T04:12:34",
            "raw_data_s3_location": "s3://one-obs-raw-data-local/NOAA_METAR/2021/11/16/04/12/299fe1114b25403e82074d1c9ffa463c.txt",
            "latitude": 27.77804,
            "longitude": -97.6878,
            "elevation": 24.0,
            "device_id": "1",
            "device_record_id": "dfba64068fdc0d0dcd6502dad0d7bcbf3f0e097f00fa9d0a941c1ba74a5b9935",
            "dtn_station_id": "1fb4d0ec-b20b-11eb-ab9c-0ad07224a539",
            "station_code": "YPXC1",
            "device_type": 69,
            "timezone": "America/Chicago",
            "geolocation_id": "88c3ae288c6612035670add39b794bf52001afa21f1b5aeee60b8620af1621eb",
            "data": [
                {
                    "source_key": "vis",
                    "value": 10.0,
                    "source_key_display": "vis",
                    "sensor_id": 0,
                    "type": "float",
                    "unit": "miles",
                    "dtn_key_id": 926,
                    "std_type": "float",
                    "std_unit": "km",
                    "std_value": 16.09,
                    "dtn_key_name": "visibility",
                    "obs_type": 29,
                    "json_doc": None,
                    "raw_data_s3_location": "s3://one-obs-raw-data-local/NOAA_METAR/2021/11/16/04/12/299fe1114b25403e82074d1c9ffa463c.txt",
                    "properties": {}
                },
                ...
            ]
            },
        ]
    """
    logger.info(data[0]['station_id'])
    for rec in data:
        pass
