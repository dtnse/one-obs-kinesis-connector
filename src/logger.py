import logging
from logging.handlers import RotatingFileHandler


FORMAT = '[%(asctime)s][%(levelname)s]%(message)s'

formatter = logging.Formatter(FORMAT)
handler = RotatingFileHandler('log/one-obs-connector.log', maxBytes=10000000, backupCount=10)
handler.setFormatter(formatter)

logger = logging.getLogger('one-obs-connector-logger')

logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
